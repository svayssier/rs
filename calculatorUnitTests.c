#include <CUnit/Basic.h>
#include <CUnit/Automated.h>
#include <CUnit/CUnit.h>

//@wi.tests RS-347
void test_addition(void) {
  CU_ASSERT(2 + 3 == 5);
  CU_ASSERT(-1 + 4 == 3);
  CU_ASSERT(0 + 0 == 0);
}
//@wi.tests http://siemensdc:8092/polarion/redirect/project/RenaultSoftware/workitem?id=RS-353
void test_subtraction(void) {
  CU_ASSERT(5 - 3 == 2);
  CU_ASSERT(-1 - (-4) == 3);
  CU_ASSERT(0 - 0 == 0);
}

void test_multiplication(void) {
  CU_ASSERT(2 * 3 == 6);
  CU_ASSERT(-1 * 4 == -4);
  CU_ASSERT(0 * 0 == 0);
}

void test_division(void) {
  CU_ASSERT(6 / 3 == 2);
  CU_ASSERT(-4 / (-1) == 4);
  CU_ASSERT(0 / 1 == 0);
}

int main() {
  CU_initialize_registry();
  CU_pSuite suite = CU_add_suite("Calculator Tests", 0, 0);

  CU_add_test(suite, "test_addition", test_addition);
  CU_add_test(suite, "test_subtraction", test_subtraction);
  CU_add_test(suite, "test_multiplication", test_multiplication);
  CU_add_test(suite, "test_division", test_division);

  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_automated_run_tests();
  CU_cleanup_registry();

  return 0;
}
